using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TutorService.Application.Contracts.Persistence;
using TutorService.Domain.TutorAggregate;

namespace TutorService.Persistence.Repositories
{
    public class TutorsRepository : BaseRepository<Tutor>, ITutorsRepository
    {
        public TutorsRepository(TutorServiceDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<Tutor>> ListAsync()
        {
            return await DbContext.Tutors
                .Where(t => t.IsActive)
                .OrderByDescending(t => t.CreatedAt)
                .ToListAsync();
        }

        public override async Task<Tutor> GetByIdAsync(Guid id)
        {
            return await DbContext.Set<Tutor>()
                .Where(t => t.Id == id)
                .Include(t => t.Certificates)
                .Include(t => t.Degrees)
                .OrderByDescending(t => t.FirstName)
                .AsSplitQuery()
                .SingleOrDefaultAsync();
        }
    }
}