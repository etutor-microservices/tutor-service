﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TutorService.Domain.TutorAggregate;

namespace TutorService.Persistence.Configuration
{
    public class TutorConfiguration : IEntityTypeConfiguration<Tutor>
    {
        public void Configure(EntityTypeBuilder<Tutor> builder)
        {
            builder.Property(t => t.FirstName).IsRequired().HasMaxLength(50);
            builder.Property(t => t.MiddleName).IsRequired().HasMaxLength(50);
            builder.Property(t => t.LastName).IsRequired().HasMaxLength(50);
            builder.Property(t => t.Gender).IsRequired().HasMaxLength(50);
            builder.Property(t => t.Description).IsRequired().HasMaxLength(250);
            builder.Property(t => t.PhoneNumber).IsRequired().HasMaxLength(20);
            builder.Property(t => t.Email).IsRequired().HasMaxLength(320);

            builder.HasIndex(t => t.PhoneNumber).IsUnique();
            builder.HasIndex(t => t.Email).IsUnique();
        }
    }
}