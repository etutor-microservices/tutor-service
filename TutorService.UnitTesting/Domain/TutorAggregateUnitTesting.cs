﻿using System;
using System.Linq;
using FluentAssertions;
using TutorService.Domain.TutorAggregate;
using Xunit;

namespace TutorService.UnitTesting.Domain
{
    public class TutorAggregateUnitTesting
    {
        private readonly Tutor _tutor = new("First", "Middle", "Last",
            "61e11a384846f7c161fb01cc", "very long description", DateTime.Parse("11/26/2000"),
            "0123456789", "tutor@gmail.com");

        [Fact]
        public void Creation_ShouldReturnExpectedTutor()
        {
            var firstName = "First";
            var middleName = "Middle";
            var lastName = "Last";
            var gender = "61e11a384846f7c161fb01cc";
            var description = "very long description";
            var dateOfBirth = DateTime.Parse("11/26/2000");
            var phoneNumber = "0123456789";
            var email = "tutor@gmail.com";

            var tutor = new Tutor(firstName, middleName, lastName, gender, description, dateOfBirth, phoneNumber,
                email);

            tutor.FirstName.Should().Be(firstName);
            tutor.MiddleName.Should().Be(middleName);
            tutor.LastName.Should().Be(lastName);
            tutor.Gender.Should().Be(gender);
            tutor.Description.Should().Be(description);
            tutor.PhoneNumber.Should().Be(phoneNumber);
            tutor.Email.Should().Be(email);
            tutor.IsActive.Should().BeFalse();
        }

        [Fact]
        public void AddDegree_ShouldAddDegree()
        {
            var name = "Bachelor of IT";
            var university = "HUFLIT";
            var major = "Information of Technology";
            var academicRank = "61e11a834846f7c161fb01db";
            var dateOfIssue = DateTime.Parse("12/12/2020");

            var degree = new Degree(name, major, university,
                dateOfIssue, academicRank);

            _tutor.AddDegree(degree);

            _tutor.Degrees.Count().Should().Be(1);
            _tutor.Degrees.First().Should().Be(degree);
        }

        [Fact]
        public void AddDegree_DuplicateDegree_ShouldNotAddDegree()
        {
            var name = "Bachelor of IT";
            var university = "HUFLIT";
            var major = "Information of Technology";
            var academicRank = "61e11a834846f7c161fb01db";
            var dateOfIssue = DateTime.Parse("12/12/2020");

            var degree = new Degree(name, major, university,
                dateOfIssue, academicRank);

            _tutor.AddDegree(degree);
            _tutor.AddDegree(degree);

            _tutor.Degrees.Count().Should().Be(1);
            _tutor.Degrees.First().Should().Be(degree);
        }

        [Fact]
        public void AddCertificate_ShouldAddCertificate()
        {
            var dateOfIssue = DateTime.Parse("11/25/2019");
            var certificate = new Certificate("IELTS", "British Council",
                dateOfIssue, dateOfIssue.AddYears(2));

            _tutor.AddCertificate(certificate);

            _tutor.Certificates.First().Should().Be(certificate);
            _tutor.Certificates.Count().Should().Be(1);
        }

        [Fact]
        public void AddCertificate_DuplicateCert_ShouldNotAddCertificate()
        {
            var dateOfIssue = DateTime.Parse("11/25/2019");
            var certificate = new Certificate("IELTS", "British Council",
                dateOfIssue, dateOfIssue.AddYears(2));

            _tutor.AddCertificate(certificate);
            _tutor.AddCertificate(certificate);

            _tutor.Certificates.First().Should().Be(certificate);
            _tutor.Certificates.Count().Should().Be(1);
        }

        [Fact]
        public void Update_ShouldReturnExpectedTutor()
        {
            var updatedFirstName = "Francis";
            _tutor.UpdateFirstName(updatedFirstName);
            _tutor.FirstName.Should().Be(updatedFirstName);

            var updatedMiddleName = "Erica";
            _tutor.UpdateMiddleName(updatedMiddleName);
            _tutor.MiddleName.Should().Be(updatedMiddleName);

            var updatedLastName = "John";
            _tutor.UpdateLastName(updatedLastName);
            _tutor.LastName.Should().Be(updatedLastName);

            var updatedPhoneNumber = "01231246162312";
            _tutor.UpdatePhoneNumber(updatedPhoneNumber);
            _tutor.PhoneNumber.Should().Be(updatedPhoneNumber);

            var updatedDescription = "Description ...";
            _tutor.UpdateDescription(updatedDescription);
            _tutor.Description.Should().Be(updatedDescription);

            var updatedEmail = "updated@gmail.com";
            _tutor.UpdateEmail(updatedEmail);
            _tutor.Email.Should().Be(updatedEmail);
        }

        [InlineData("")]
        [InlineData("First")]
        [Theory]
        public void Update_InvalidParams_ShouldNotUpdateFirstName(string updatedFirstName)
        {
            _tutor.UpdateFirstName(updatedFirstName);
            _tutor.FirstName.Should().Be(_tutor.FirstName);
        }

        [Fact]
        public void Deactivate_ShouldSetIsActiveToFalse()
        {
            _tutor.Deactivate();
            _tutor.IsActive.Should().BeFalse();
        }

        [Fact]
        public void Activate_ShouldSetIsActiveToFalse()
        {
            _tutor.Activate();
            _tutor.IsActive.Should().BeTrue();
        }

        [Fact]
        public void Unregister_DeletedAtShouldNotBeNull()
        {
            var success = _tutor.Unregister();

            success.Should().BeTrue();
            _tutor.DeletedAt.Should().NotBeNull();
        }

        [Fact]
        public void Unregister_ActiveTutor_ShouldReturnFalseToUnregister()
        {
            _tutor.Activate();
            var success = _tutor.Unregister();

            success.Should().BeFalse();
            _tutor.DeletedAt.Should().BeNull();
        }
    }
}