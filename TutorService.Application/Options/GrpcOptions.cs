namespace TutorService.Application.Options
{
    public class GrpcOptions
    {
        public const string Grpc = "GrpcServices";

        public string StorageService { get; init; }
    }
}