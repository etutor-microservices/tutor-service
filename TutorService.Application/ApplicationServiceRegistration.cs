﻿using System;
using System.Reflection;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TutorService.Application.Options;

namespace TutorService.Application
{
    public static class ApplicationServiceRegistration
    {
        public static void AddApplicationService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            var grpcOptions = new GrpcOptions();
            configuration.GetSection(GrpcOptions.Grpc).Bind(grpcOptions);

            services.AddGrpcClient<FilesService.FilesServiceClient>(opt =>
            {
                opt.Address = new Uri(grpcOptions.StorageService);
            });
        }
    }
}