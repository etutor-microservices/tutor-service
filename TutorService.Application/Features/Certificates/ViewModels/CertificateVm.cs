using System;

namespace TutorService.Application.Features.Certificates.ViewModels
{
    public class CertificateVm
    {
        public string Name { get; set; }
        public string PlaceOfIssue { get; set; }
        public DateTime DateOfIssue { get; set; }
        public DateTime ExpiresIn { get; set; }
    }
}