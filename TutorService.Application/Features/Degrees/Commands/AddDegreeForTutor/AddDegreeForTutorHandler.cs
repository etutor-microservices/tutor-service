using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using TutorService.Application.Contracts.Persistence;
using TutorService.Application.Features.Degrees.ViewModels;
using TutorService.Domain.TutorAggregate;

namespace TutorService.Application.Features.Degrees.Commands.AddDegreeForTutor
{
    public class
        AddDegreeForTutorHandler : IRequestHandler<AddDegreeForTutor, (List<ValidationFailure> errors, DegreeVm degree)>
    {
        private readonly ITutorsRepository _tutorsRepository;
        private readonly IValidator<AddDegreeForTutor> _validator;
        private readonly IMapper _mapper;

        public AddDegreeForTutorHandler(ITutorsRepository tutorsRepository, IValidator<AddDegreeForTutor> validator,
            IMapper mapper)
        {
            _tutorsRepository = tutorsRepository ?? throw new ArgumentNullException(nameof(tutorsRepository));
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<(List<ValidationFailure> errors, DegreeVm degree)> Handle(AddDegreeForTutor request,
            CancellationToken cancellationToken)
        {
            var validationResult = await _validator.ValidateAsync(request, cancellationToken);
            if (!validationResult.IsValid) return (validationResult.Errors, null);

            var degree = new Degree(request.Name, request.Major, request.GraduatedUniversity, request.DateOfIssue,
                request.AcademicRank);
            request.Tutor.AddDegree(degree);

            await _tutorsRepository.UpdateAsync(request.Tutor);
            return (null, _mapper.Map<DegreeVm>(degree));
        }
    }
}