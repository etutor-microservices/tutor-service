using System;
using Google.Protobuf.Collections;

namespace TutorService.Application.Features.Tutors.ViewModels
{
    public class TutorVm
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public RepeatedField<File> Images { get; set; }
    }
}