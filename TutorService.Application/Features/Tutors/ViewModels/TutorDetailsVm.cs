using System;
using System.Collections.Generic;
using Google.Protobuf.Collections;
using TutorService.Application.Features.Certificates.ViewModels;
using TutorService.Application.Features.Degrees.ViewModels;

namespace TutorService.Application.Features.Tutors.ViewModels
{
    public class TutorDetailsVm
    {
        public Guid Id { get; init; }
        public string FirstName { get; init; }
        public string MiddleName { get; init; }
        public string LastName { get; init; }
        public string Gender { get; init; }
        public string Description { get; init; }
        public DateTime DateOfBirth { get; init; }
        public string PhoneNumber { get; init; }
        public string Email { get; init; }
        public IEnumerable<CertificateVm> Certificates { get; init; }
        public IEnumerable<DegreeVm> Degrees { get; init; }
        public RepeatedField<File> Images { get; set; }
    }
}