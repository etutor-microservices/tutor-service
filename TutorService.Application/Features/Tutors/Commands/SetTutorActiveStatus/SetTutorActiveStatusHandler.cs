using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using TutorService.Application.Contracts;
using TutorService.Application.Contracts.Persistence;
using TutorService.Domain.TutorAggregate;

namespace TutorService.Application.Features.Tutors.Commands.SetTutorActiveStatus
{
    public class SetTutorActiveStatusHandler : IRequestHandler<SetTutorActiveStatus, Tutor>
    {
        private readonly ITutorsRepository _tutorsRepository;

        public SetTutorActiveStatusHandler(ITutorsRepository tutorsRepository)
        {
            _tutorsRepository = tutorsRepository ?? throw new ArgumentNullException(nameof(tutorsRepository));
        }

        public async Task<Tutor> Handle(SetTutorActiveStatus request,
            CancellationToken cancellationToken)
        {
            if (request.IsActive) request.Tutor.Activate();
            else request.Tutor.Deactivate();

            await _tutorsRepository.UpdateAsync(request.Tutor);

            return request.Tutor;
        }
    }
}