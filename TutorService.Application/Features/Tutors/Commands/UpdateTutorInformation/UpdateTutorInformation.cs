using System;
using System.Collections.Generic;
using FluentValidation.Results;
using MediatR;
using TutorService.Application.Features.Tutors.ViewModels;
using TutorService.Domain.TutorAggregate;

namespace TutorService.Application.Features.Tutors.Commands.UpdateTutorInformation
{
    public class UpdateTutorInformation : IRequest<(List<ValidationFailure> errors, TutorDetailsVm tutor)>
    {
        public Tutor Tutor { get; set; }
        public string FirstName { get; init; }
        public string MiddleName { get; init; }
        public string LastName { get; init; }
        public string Gender { get; init; }
        public string Description { get; init; }
        public DateTime DateOfBirth { get; init; }
        public string PhoneNumber { get; init; }
        public string Email { get; init; }
    }
}