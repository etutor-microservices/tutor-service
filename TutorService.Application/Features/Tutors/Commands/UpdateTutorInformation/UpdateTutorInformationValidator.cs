using FluentValidation;

namespace TutorService.Application.Features.Tutors.Commands.UpdateTutorInformation
{
    public class UpdateTutorInformationValidator : AbstractValidator<UpdateTutorInformation>
    {
        public UpdateTutorInformationValidator()
        {
            RuleFor(u => u.Tutor).NotNull();
            RuleFor(u => u.FirstName).MinimumLength(2).MaximumLength(50).When(t => !string.IsNullOrEmpty(t.FirstName));
            RuleFor(u => u.MiddleName).MinimumLength(2).MaximumLength(50)
                .When(t => !string.IsNullOrEmpty(t.MiddleName));
            RuleFor(u => u.LastName).MinimumLength(2).MaximumLength(50).When(t => !string.IsNullOrEmpty(t.LastName));
            RuleFor(u => u.Description).MaximumLength(250).When(t => !string.IsNullOrEmpty(t.Description));
            RuleFor(u => u.PhoneNumber).MinimumLength(10).MaximumLength(20)
                .When(t => !string.IsNullOrEmpty(t.PhoneNumber));
            RuleFor(u => u.Email).EmailAddress().When(t => !string.IsNullOrEmpty(t.Email));
            RuleFor(u => u.Gender).MaximumLength(50).NotEmpty().When(u => !string.IsNullOrEmpty(u.Gender));
        }
    }
}