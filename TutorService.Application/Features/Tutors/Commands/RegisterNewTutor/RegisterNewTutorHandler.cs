using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using TutorService.Application.Contracts.Infrastructure;
using TutorService.Application.Contracts.Persistence;
using TutorService.Application.Features.Tutors.ViewModels;
using TutorService.Domain.TutorAggregate;

namespace TutorService.Application.Features.Tutors.Commands.RegisterNewTutor
{
    public class RegisterNewTutorHandler : IRequestHandler<RegisterNewTutor, (List<ValidationFailure> errors,
        TutorDetailsVm tutor)>
    {
        private readonly ITutorsRepository _tutorsRepository;
        private readonly IValidator<RegisterNewTutor> _validator;
        private readonly IMessagingService _messagingService;
        private readonly IMapper _mapper;

        public RegisterNewTutorHandler(ITutorsRepository tutorsRepository, IValidator<RegisterNewTutor> validator,
            IMessagingService messagingService, IMapper mapper)
        {
            _tutorsRepository = tutorsRepository ?? throw new ArgumentNullException(nameof(tutorsRepository));
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
            _messagingService = messagingService ?? throw new ArgumentNullException(nameof(messagingService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<(List<ValidationFailure> errors, TutorDetailsVm tutor)> Handle(RegisterNewTutor request,
            CancellationToken cancellationToken)
        {
            var validationResult = await _validator.ValidateAsync(request, cancellationToken);
            if (!validationResult.IsValid) return (validationResult.Errors, null);

            var tutor = new Tutor(request.FirstName, request.MiddleName, request.LastName, request.Gender,
                request.Description, request.DateOfBirth, request.PhoneNumber, request.Email);

            var insertedTutor = await _tutorsRepository.AddAsync(tutor);
            await _messagingService.UploadImages(insertedTutor.Id, request.Files);

            return (null, _mapper.Map<TutorDetailsVm>(tutor));
        }
    }
}