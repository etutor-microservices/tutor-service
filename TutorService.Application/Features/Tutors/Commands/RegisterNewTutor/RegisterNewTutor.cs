using System;
using System.Collections.Generic;
using FluentValidation.Results;
using MediatR;
using Microsoft.AspNetCore.Http;
using TutorService.Application.Features.Tutors.ViewModels;

namespace TutorService.Application.Features.Tutors.Commands.RegisterNewTutor
{
    public class RegisterNewTutor : IRequest<(List<ValidationFailure> errors, TutorDetailsVm tutor)>
    {
        public string FirstName { get; init; }
        public string MiddleName { get; init; }
        public string LastName { get; init; }
        public string Gender { get; init; }
        public string Description { get; init; }
        public DateTime DateOfBirth { get; init; }
        public string PhoneNumber { get; init; }
        public string Email { get; init; }
        public List<IFormFile> Files { get; init; }
    }
}