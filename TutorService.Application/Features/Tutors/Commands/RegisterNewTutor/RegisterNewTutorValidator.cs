using System;
using FluentValidation;

namespace TutorService.Application.Features.Tutors.Commands.RegisterNewTutor
{
    public class RegisterNewTutorValidator : AbstractValidator<RegisterNewTutor>
    {
        public RegisterNewTutorValidator()
        {
            RuleFor(rnt => rnt.FirstName).NotEmpty().MinimumLength(2).MaximumLength(50);
            RuleFor(rnt => rnt.MiddleName).NotEmpty().MinimumLength(2).MaximumLength(50);
            RuleFor(rnt => rnt.LastName).NotEmpty().MinimumLength(2).MaximumLength(50);
            RuleFor(rnt => rnt.DateOfBirth).Must(dob => DateTime.UtcNow.Year - dob.Year >= 18)
                .WithMessage("Tutor has to be over 18 years old.");
            RuleFor(rnt => rnt.Description).NotEmpty().MaximumLength(250);
            RuleFor(rnt => rnt.Email).NotEmpty().EmailAddress();
            RuleFor(rnt => rnt.PhoneNumber).NotEmpty().MinimumLength(10).MaximumLength(20);
            RuleFor(rnt => rnt.Gender).NotEmpty().NotNull().MaximumLength(50);
        }
    }
}