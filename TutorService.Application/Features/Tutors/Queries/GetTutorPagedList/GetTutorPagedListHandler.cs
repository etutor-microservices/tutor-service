﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using TutorService.Application.Contracts;
using TutorService.Application.Contracts.Infrastructure;
using TutorService.Application.Contracts.Persistence;
using TutorService.Application.Features.Tutors.ViewModels;
using TutorService.Application.Shared.Responses;
using TutorService.Domain.TutorAggregate;

namespace TutorService.Application.Features.Tutors.Queries.GetTutorPagedList
{
    public class GetTutorPagedListHandler : IRequestHandler<GetTutorPagedList, PagedList<TutorVm>>
    {
        private readonly ITutorsRepository _tutorsRepository;
        private readonly IMapper _mapper;
        private readonly IStorageService _storageService;

        public GetTutorPagedListHandler(ITutorsRepository tutorsRepository, IMapper mapper,
            IStorageService storageService)
        {
            _tutorsRepository = tutorsRepository ??
                                throw new ArgumentNullException(nameof(tutorsRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _storageService = storageService ?? throw new ArgumentNullException(nameof(storageService));
        }

        public async Task<PagedList<TutorVm>> Handle(GetTutorPagedList request,
            CancellationToken cancellationToken)
        {
            var tutorList = await _tutorsRepository.ListAsync();

            var tutorVmPagedList = new PagedList<TutorVm>(_mapper.Map<List<TutorVm>>(tutorList), request.PageNumber,
                request.PageSize);

            foreach (var tutor in tutorVmPagedList)
            {
                var imageList = await _storageService.GetImageListForTutor(
                    new GetAllForOwnerRequest {OwnerId = tutor.Id.ToString()});

                tutor.Images = imageList.Files;
            }

            return tutorVmPagedList;
        }
    }
}