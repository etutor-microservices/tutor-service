﻿using MediatR;
using TutorService.Application.Features.Tutors.ViewModels;
using TutorService.Application.Shared.Requests;
using TutorService.Application.Shared.Responses;
using TutorService.Domain.TutorAggregate;

namespace TutorService.Application.Features.Tutors.Queries.GetTutorPagedList
{
    public class GetTutorPagedList : GetPagedList, IRequest<PagedList<TutorVm>>
    {
    }
}