using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using TutorService.Application.Contracts;
using TutorService.Application.Contracts.Infrastructure;
using TutorService.Application.Contracts.Persistence;
using TutorService.Application.Features.Tutors.ViewModels;
using TutorService.Domain.TutorAggregate;

namespace TutorService.Application.Features.Tutors.Queries.GetTutorDetails
{
    public class GetTutorDetailsHandler : IRequestHandler<GetTutorDetails, TutorDetailsVm>
    {
        private readonly ITutorsRepository _tutorsRepository;
        private readonly IStorageService _storageService;
        private readonly IMapper _mapper;

        public GetTutorDetailsHandler(ITutorsRepository tutorsRepository, IStorageService storageService,
            IMapper mapper)
        {
            _tutorsRepository = tutorsRepository ?? throw new ArgumentNullException(nameof(tutorsRepository));
            _storageService = storageService ?? throw new ArgumentNullException(nameof(storageService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<TutorDetailsVm> Handle(GetTutorDetails request, CancellationToken cancellationToken)
        {
            var tutor = await _tutorsRepository.GetByIdAsync(request.Id);
            if (tutor == null) return null;

            var tutorDetailsVm = _mapper.Map<TutorDetailsVm>(tutor);

            var imageList =
                await _storageService.GetImageListForTutor(new GetAllForOwnerRequest() {OwnerId = tutor.Id.ToString()});

            tutorDetailsVm.Images = imageList.Files;

            return tutorDetailsVm;
        }
    }
}