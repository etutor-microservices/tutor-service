using AutoMapper;
using TutorService.Application.Features.Certificates.ViewModels;
using TutorService.Application.Features.Degrees.ViewModels;
using TutorService.Application.Features.Tutors.ViewModels;
using TutorService.Domain.TutorAggregate;

namespace TutorService.Application.MappingProfile
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Tutor, TutorVm>();
            CreateMap<Tutor, TutorDetailsVm>();

            CreateMap<Certificate, CertificateVm>();

            CreateMap<Degree, DegreeVm>();
        }
    }
}