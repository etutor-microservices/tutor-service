using System.Collections.Generic;

namespace TutorService.Application.Contracts.Infrastructure
{
    public interface UploadTutorImageList
    {
        public string OwnerId { get; }
        public List<UploadImage> Files { get; }
    }

    public class UploadImage
    {
        public byte[] Buffer { get; set; }
        public string Filename { get; set; }
        public string Mimetype { get; set; }
    }
}