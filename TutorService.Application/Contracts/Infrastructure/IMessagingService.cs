using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace TutorService.Application.Contracts.Infrastructure
{
    public interface IMessagingService
    {
        Task UploadImages(Guid tutorId, IEnumerable<IFormFile> files);
    }
}