using System.Threading.Tasks;

namespace TutorService.Application.Contracts.Infrastructure
{
    public interface IStorageService
    {
        Task<GetAllForOwnerResponse> GetImageListForTutor(GetAllForOwnerRequest request);
    }
}