﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TutorService.Domain.TutorAggregate;

namespace TutorService.Application.Contracts.Persistence
{
    public interface ITutorsRepository : IAsyncReadRepository<Tutor>, IAsyncRepository<Tutor>
    {
        Task<List<Tutor>> ListAsync();
    }
}