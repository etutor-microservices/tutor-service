using System;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using TutorService.Application.Contracts.Infrastructure;
using TutorService.Infrastructure.Options;
using TutorService.Infrastructure.Services;

namespace TutorService.Infrastructure
{
    public static class InfrastructureServiceRegistration
    {
        public static void AddInfrastructureService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IStorageService, StorageService>();
            services.AddScoped<IMessagingService, MessagingService>();

            var rabbitMqOptions = new RabbitMqOptions();
            configuration.GetSection(RabbitMqOptions.RabbitMq).Bind(rabbitMqOptions);

            services.AddMassTransit(x =>
            {
                x.AddBus(_ => Bus.Factory.CreateUsingRabbitMq(config =>
                {
                    config.Host(new Uri(rabbitMqOptions.Host), h =>
                    {
                        h.Username(rabbitMqOptions.Username);
                        h.Password(rabbitMqOptions.Password);
                    });

                    config.Publish<UploadTutorImageList>(p =>
                    {
                        p.BindQueue("storage", "storage-upload-queue", q =>
                        {
                            q.RoutingKey = "storage.command.upload";
                            q.ExchangeType = ExchangeType.Direct;
                        });
                    });
                }));
            });

            services.AddMassTransitHostedService();
        }
    }
}