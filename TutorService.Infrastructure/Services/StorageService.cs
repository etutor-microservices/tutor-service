using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Grpc.Core;
using TutorService.Application;
using TutorService.Application.Contracts.Infrastructure;

namespace TutorService.Infrastructure.Services
{
    public class StorageService : IStorageService
    {
        private readonly FilesService.FilesServiceClient _client;

        public StorageService(FilesService.FilesServiceClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<GetAllForOwnerResponse> GetImageListForTutor(GetAllForOwnerRequest request)
        {
            try
            {
                return await _client.GetAllForOwnerAsync(request);
            }
            catch (RpcException)
            {
                return new GetAllForOwnerResponse {Files = {new List<File>()}};
            }
        }
    }
}