using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.Http;
using TutorService.Application.Contracts.Infrastructure;

namespace TutorService.Infrastructure.Services
{
    public class MessagingService : IMessagingService
    {
        private readonly IBusControl _busControl;

        public MessagingService(IBusControl busControl)
        {
            _busControl = busControl ?? throw new ArgumentNullException(nameof(busControl));
        }

        public async Task UploadImages(Guid tutorId, IEnumerable<IFormFile> files)
        {
            var uploadingImageList = new List<UploadImage>();

            foreach (var file in files)
            {
                await using var ms = new MemoryStream();
                await file.CopyToAsync(ms);

                uploadingImageList.Add(new UploadImage
                {
                    Filename = file.FileName,
                    Buffer = ms.ToArray(),
                    Mimetype = file.ContentType
                });
            }

            await _busControl.Publish<UploadTutorImageList>(new
            {
                OwnerId = tutorId,
                Files = uploadingImageList
            });
        }
    }
}