﻿using System;
using System.Collections.Generic;
using TutorService.Domain.Common;

namespace TutorService.Domain.TutorAggregate
{
    public class Tutor : AuditableEntity
    {
        public Tutor(string firstName, string middleName, string lastName, string gender, string description,
            DateTime dateOfBirth, string phoneNumber, string email)
        {
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            Gender = gender;
            Description = description;
            DateOfBirth = dateOfBirth;
            PhoneNumber = phoneNumber;
            Email = email;
            IsActive = false;
        }

        public string FirstName { get; private set; }
        public string MiddleName { get; private set; }
        public string LastName { get; private set; }
        public string Gender { get; private set; }
        public string Description { get; private set; }
        public DateTime DateOfBirth { get; private set; }
        public string PhoneNumber { get; private set; }
        public string Email { get; private set; }
        public bool IsActive { get; private set; }

        private readonly List<Certificate> _certificates = new();
        public IEnumerable<Certificate> Certificates => _certificates.AsReadOnly();

        private readonly List<Degree> _degrees = new();
        public IEnumerable<Degree> Degrees => _degrees.AsReadOnly();

        public void AddCertificate(Certificate certificate)
        {
            if (_certificates.Contains(certificate)) return;
            _certificates.Add(certificate);
        }

        public void AddDegree(Degree degree)
        {
            if (_degrees.Contains(degree)) return;
            _degrees.Add(degree);
        }

        public void UpdateFirstName(string firstName)
        {
            if (ValidateUpdatedStringField(FirstName, firstName)) return;
            FirstName = firstName;
        }

        public void UpdateMiddleName(string middleName)
        {
            if (ValidateUpdatedStringField(MiddleName, middleName)) return;
            MiddleName = middleName;
        }

        public void UpdateLastName(string lastName)
        {
            if (ValidateUpdatedStringField(LastName, lastName)) return;
            LastName = lastName;
        }

        public void UpdatePhoneNumber(string phoneNumber)
        {
            if (ValidateUpdatedStringField(PhoneNumber, phoneNumber)) return;
            PhoneNumber = phoneNumber;
        }

        public void UpdateDescription(string description)
        {
            if (ValidateUpdatedStringField(Description, description)) return;
            Description = description;
        }

        public void UpdateEmail(string email)
        {
            if (ValidateUpdatedStringField(Email, email)) return;
            Email = email;
        }

        public void UpdateGender(string gender)
        {
            Gender = gender;
        }

        public void Deactivate()
        {
            if (!IsActive) return;
            IsActive = false;
        }

        public void Activate()
        {
            if (IsActive) return;
            IsActive = true;
        }

        public bool Unregister()
        {
            if (IsActive) return false;
            DeletedAt = DateTime.UtcNow;
            return true;
        }

        private bool ValidateUpdatedStringField(string current, string updated)
        {
            return string.IsNullOrEmpty(updated) || current == updated;
        }
    }
}