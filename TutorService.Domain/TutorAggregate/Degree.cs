﻿using System;
using TutorService.Domain.Common;

namespace TutorService.Domain.TutorAggregate
{
    public class Degree : BaseEntity<int>
    {
        public Degree(string name, string major, string graduatedUniversity, DateTime dateOfIssue, string academicRank)
        {
            Name = name;
            Major = major;
            GraduatedUniversity = graduatedUniversity;
            DateOfIssue = dateOfIssue;
            AcademicRank = academicRank;
        }

        public string Name { get; private set; }
        public string Major { get; private set; }
        public string GraduatedUniversity { get; private set; }
        public DateTime DateOfIssue { get; private set; }
        public string AcademicRank { get; private set; }

        public void UpdateName(string name)
        {
            if (string.IsNullOrEmpty(name)) return;
            Name = name;
        }

        public void UpdateMajor(string major)
        {
            if (string.IsNullOrEmpty(major)) return;
            Major = major;
        }

        public void UpdateGraduatedUniversity(string graduatedUniversity)
        {
            if (string.IsNullOrEmpty(graduatedUniversity)) return;
            GraduatedUniversity = graduatedUniversity;
        }

        public void UpdateDateOfIssue(DateTime dateOfIssue)
        {
            if (dateOfIssue == default) return;
            DateOfIssue = dateOfIssue;
        }

        public void UpdateAcademicRank(string academicRank)
        {
            AcademicRank = academicRank;
        }
    }
}