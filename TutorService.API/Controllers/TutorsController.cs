using System;
using System.Text.Json;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using TutorService.Application.Features.Tutors.Commands.RegisterNewTutor;
using TutorService.Application.Features.Tutors.Commands.SetTutorActiveStatus;
using TutorService.Application.Features.Tutors.Commands.UnregisterTutor;
using TutorService.Application.Features.Tutors.Commands.UpdateTutorInformation;
using TutorService.Application.Features.Tutors.Queries.GetTutorById;
using TutorService.Application.Features.Tutors.Queries.GetTutorDetails;
using TutorService.Application.Features.Tutors.Queries.GetTutorPagedList;

namespace TutorService.API.Controllers
{
    [ApiController]
    [Route("tutors")]
    public class TutorsController : BaseController
    {
        private readonly IMediator _mediator;

        public TutorsController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet]
        public async Task<IActionResult> GetPagedList([FromQuery] GetTutorPagedList getTutorPagedList)
        {
            var tutorList = await _mediator.Send(getTutorPagedList);

            Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(new
            {
                tutorList.TotalCount,
                tutorList.CurrentPage,
                tutorList.PageSize,
                tutorList.TotalPage
            }, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            }));

            Response.Headers.Add("Access-Control-Expose-Headers", "X-Pagination");

            return Ok(tutorList);
        }

        [HttpGet("{id:guid}", Name = "GetDetails")]
        public async Task<IActionResult> GetDetails([FromRoute] GetTutorDetails getTutorDetails)
        {
            var tutor = await _mediator.Send(getTutorDetails);
            if (tutor == null) return NotFound();
            return Ok(tutor);
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromForm] RegisterNewTutor registerNewTutor)
        {
            var (errors, tutor) = await _mediator.Send(registerNewTutor);
            if (errors != null) return BadRequest(errors);
            return CreatedAtRoute("GetDetails", new {id = tutor.Id}, tutor);
        }

        [HttpPatch("{id:guid}")]
        public async Task<IActionResult> Update([FromRoute] Guid id,
            [FromBody] UpdateTutorInformation updateTutorInformation)
        {
            var tutor = await _mediator.Send(new GetTutorById {Id = id});
            if (tutor == null) return NotFound();

            updateTutorInformation.Tutor = tutor;
            var (errors, updatedTutor) = await _mediator.Send(updateTutorInformation);
            if (errors != null) return BadRequest(errors);

            return Ok(updatedTutor);
        }

        [HttpPatch("{id:guid}/is-active")]
        public async Task<IActionResult> Deactivate([FromRoute] Guid id,
            [FromBody] SetTutorActiveStatus setTutorActiveStatus)
        {
            var tutor = await _mediator.Send(new GetTutorById {Id = id});
            if (tutor == null) return NotFound($"Tutor with id {id} cannot be found");

            setTutorActiveStatus.Tutor = tutor;
            var updatedTutor = await _mediator.Send(setTutorActiveStatus);

            return Ok(updatedTutor);
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Unregister([FromRoute] Guid id)
        {
            var tutor = await _mediator.Send(new GetTutorById {Id = id});
            if (tutor == null) return NotFound($"Tutor with id {id} cannot be found");

            var success = await _mediator.Send(new UnregisterTutor {Tutor = tutor});
            if (!success) return BadRequest("Cannot unregister an active tutor");

            return NoContent();
        }
    }
}