using System.Collections.Generic;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;

namespace TutorService.API.Controllers
{
    public class BaseController : ControllerBase
    {
        public BadRequestObjectResult BadRequest(List<ValidationFailure> errors)
        {
            var errorMessages = new List<string>();

            errors.ForEach(error => errorMessages.Add(error.ErrorMessage));

            return base.BadRequest(errorMessages);
        }
    }
}